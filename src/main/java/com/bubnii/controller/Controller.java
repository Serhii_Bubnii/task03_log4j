package com.bubnii.controller;

import org.apache.logging.log4j.*;

import static com.bubnii.controller.ClassName.getCurrentClassName;

public class Controller {
    private static final Logger logger = LogManager.getLogger(getCurrentClassName());

    public void testController() {
        logger.info("Body of the method");
        try {
            logger.error("Throw an InterruptedException");
            throw new InterruptedException();
        } catch (InterruptedException e) {
            logger.info("Caught an exception" + e);
        } finally {
            logger.trace("Test trace Logger");
            logger.debug("Test debug Logger");
            logger.info("Finally bloc an exception");
        }
    }
}

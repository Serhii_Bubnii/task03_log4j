package com.bubnii.view;

import com.bubnii.controller.Controller;
import com.bubnii.model.Model;
import org.apache.logging.log4j.*;

import java.io.IOException;

import static com.bubnii.controller.ClassName.getCurrentClassName;

public class View {

    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private Model model = new Model();
    private Controller controller = new Controller();

    public void testView() {
        logger.info("Body of the method");
        try {
            logger.warn("Throw an IOException");
            throw new IOException();
        } catch (IOException e) {
            logger.info("Caught an exception" + e);
        } finally {
            logger.fatal("Finally bloc an exception");
            logger.info("Start method testModel()");
            model.testModel();
            logger.info("Start method testController()");
            controller.testController();
        }
    }
}

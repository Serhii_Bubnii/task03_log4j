package com.bubnii.model;

import org.apache.logging.log4j.*;

import java.sql.SQLException;

import static com.bubnii.controller.ClassName.getCurrentClassName;

public class Model {
    private static final Logger logger = LogManager.getLogger(getCurrentClassName());

    public void testModel() {
        logger.info("Body of the method");
        try {
            logger.error("Throw an SQLException");
            throw new SQLException();
        } catch (SQLException e) {
            logger.info("Caught an exception" + e);
            logger.trace("Test trace Logger" + e);
            logger.debug("Test debug Logger" + e);
        } finally {
            logger.error("Finally bloc an exception");
            logger.trace("Test trace Logger");
            logger.debug("Test debug Logger");
        }
    }
}

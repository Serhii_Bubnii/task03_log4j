package com.bubnii;

import com.bubnii.view.View;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.bubnii.controller.ClassName.getCurrentClassName;

public class Main {
    private static final Logger logger = LogManager.getLogger(getCurrentClassName());
    private View view = new View();

    public void testController() {
        logger.fatal("fatal");
        view.testView();
        logger.debug("testController() method");
        logger.info("testController() method");
    }

    public static void main(String[] args) {
        try {
            for (int i = 0; i < 1; i++) {
                logger.info("fatal");
                new Main().testController();
            }
        } finally {
            logger.trace("finally bloc on main() method");
            logger.debug("finally bloc on main() method");
            logger.info("finally bloc on main() method");
            logger.warn("finally bloc on main() method");
            logger.error("finally bloc on main() method");
            logger.fatal("finally bloc on main() method");
        }
    }
}
